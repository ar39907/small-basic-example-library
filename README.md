# Small Basic Example Library (2007 & 2011)



## Description

Archived Small Basic examples from 2007 & 2011.


## Getting Started

### Dependencies

* Small Basic Software
* Windows


### Usage

* Compile .sb file
* Run matching .exe file


## Author

Austin Richie

https://gitlab.com/ar39907/
